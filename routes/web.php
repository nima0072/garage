<?php

use App\Http\Controllers\CarController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\dealersController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\MainController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// main page
Route::get('/', [MainController::class, 'index'])->name("main");

// cars
Route::get('/cars', [CarController::class, 'search_car'])->name("cars");
Route::get('/cars/create', [CarController::class, 'create'])->name("cars.create");
Route::post('/cars/store', [CarController::class, 'store'])->name("cars.store");
Route::get('/cars/filter/{car}', [CarController::class, 'search_car'])->name("cars.search");
Route::get('/cars/{car}', [CarController::class , 'show'])->name("cars.show");


// dealers
Route::get('/dealers', [dealersController::class, "index"])->name('dealers');

// images
Route::get('/images/{name}', [ImageController::class, 'getImageByName']);

//contact
Route::get('/contact', [ContactController::class, 'index']);
Route::post('/contact/send-email', [ContactController::class, 'emailSender'])->name('send_email');

// 


