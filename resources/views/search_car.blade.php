@extends('search_layout')

{{-- @section('header')
    <link rel="stylesheet" type="text/css" href="/css/source/bootstrap-5.0.2-dist/css/bootstrap.css">
@endsection --}}

@section('search_bar')
    <nav class="navbar navbar-default midle-nav">

        <div class="collapse navbar-collapse" id="navbarmidle" style="padding-top: 10px">

            <form action="{{ route('cars') }}" method="GET">

                <ul class="nav navbar-nav navbarborder">
                    <li class="li-category" style="padding-top:10px;margin-top:20px">
                        <div class="form-group">
                            <input id="car_name" type="text" class="form-control searchform" placeholder="Enter Keyword">
                        </div>

                    </li>
                    <li class="li-category">
                        <div class="dropdown-toggle slidertxt" style="padding-bottom:10px">Category :</div>
                        <a class="btn dropdown-toggle btn-costume category" id="dropdownMenu1" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="true">Choose<span
                                class="glyphicon glyphicon-chevron-down downicon"></span></a>

                        <ul class="dropdown-menu" id="mydd">
                            <a href="?category=sport">
                                <li>Sport</li>
                            </a>
                            <a href="?category=special">
                                <li>Special</li>
                            </a>
                            <a href="?category=old">
                                <li>Old</li>
                            </a>
                        </ul>
                    </li>
                    <li class="li-maxyear">
                        <div class="dropdown-toggle slidertxt" style="padding-bottom:10px">Minimum Top speed :</div>

                        <a class="btn dropdown-toggle btn-costume" id="dropdownMenu3" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="true"><span
                                class="glyphicon glyphicon-chevron-down downicon"></span></a>
                        <ul class="dropdown-menu" id="mydd3">
                            <li><a class="speeds" href="#">100</a></li>
                            <li><a class="speeds" href="#">200</a></li>
                            <li><a class="speeds" href="#">300</a></li>
                            <li><a class="speeds" href="#">400</a></li>
                            <input id="topspeed" type="text" hidden>
                        </ul>
                    </li>
                    <li class="li-slideprice">

                        <label class="slidertxt price" for="search_input">Min Price :</label>
                        <input id="min_price" type="text" class="form-control form-control-sm" name="search_input"
                            id="search_input">

                    </li>
                    <li class="li-slideprice">
                        <label class="slidertxt price" for="search_input">Max Price :</label>
                        <input id="max_price" type="text" class="form-control form-control-sm" name="search_input"
                            id="search_input">
                    </li>

                    <li class="li-search">
                        <button id="search" class="searchbutton">
                            <a href="{{ route('cars') }}"> <span class="glyphicon glyphicon-search "></span></a>
                        </button>
                    </li>
                </ul>
            </form>

        </div>
    </nav>
@endsection

@section('products')
    <div class="container">
        @forelse ($cars as $car)

            <div class="car">
                <img class="image" src=" {{ $car->imgPath }}" alt="image">
                <div>
                    <h3 class="">{{ $car->fullName }}</h3>
                        <p class="">${{ $car->Price }}</p>
                        <button class="btn btn-lg btn-success">
                            <a class="link" href="/cars/{{ $car->id }}/buy">
                                Add to card
                            </a>
                        </button>
                        <button class="btn btn-lg btn-primary"><a class="link"
                                href="{{ route('cars.show', $car) }}">Specifications</a></button>
                </div>
            </div>
        @empty
            <div>nothing to show!!
                <br> 
                change filters
            </div>
        @endforelse
    </div>
    

    <div class="container">
        <div class="row items-center">

            @for ($i = 1; $i <= $pages; $i++)
                <a name="" id="" class="btn btn-primary" href="?page={{ $i }}" role="button"> {{ $i }}
                </a>
            @endfor

        </div>
    </div>
    {{ $cars->onEachSide($pages)->links() }}
    </div>
    <style>
        .link,
        .link:hover {
            color: inherit;
            text-decoration: none;
        }

        .car {
            padding: 2rem;
            display: inline-block;
        }

        .image {
            width: 300px;
            max-height: 200px;
            border: 1px solid red;
            padding: 1rem;
        }

        #mydd li {
            text-align: center;
            padding: 5px 0px 5px 0px;
            color: black;
        }

        #mydd {
            padding: 0px;
            position: relative;
        }

        #mydd a {
            text-decoration: none;
        }

        #mydd3 {
            max-width: 50px !important;
            padding: 0px
        }

        .li-slideprice {
            width: 190px;

        }

        .li-search {
            margin: 20px 10px 10px 0px !important;
        }

        #search_input {
            margin-top: 10px;
        }

        .li-category {
            width: 200px;
            max-height: 50px !important;
        }

        #dropdownMenu1 {
            max-height: 45px;
        }

    </style>

@endsection

@section('js')
    <script>
        $('.speeds').click(function() {
            text = this.html();
            $('#topspeed').val(text);
        });

        $('.price').change(function() {
            text = this.html();

        });


        $('search').click(function() {
            var category = $('.category li').html();
            var car_name = $('#car_name').html();
            var topspeed = $('#topspeed').html();
            var min_price = $('#min_price').html();
            var max_price = $('#max_price').html();

            $.ajax({
                url: "/images/" + image,
                method: "get",
                dataType: "text",
                contentType: "application/json",
                success: function(res) {
                    res = res.replace(/"/g, '');
                    load_image(res, "carimage");
                },
                error: function(a, b, error) {
                    alert(error);
                }
            });

        })
    </script>
@endsection
