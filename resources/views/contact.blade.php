@extends('layout')
@section('header')
<link rel="stylesheet" href="css/style/contactstyle.css">
@endsection

@section('logo')
<div class="container">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed toggle-costume" data-toggle="collapse"
			data-target="#upmenu" aria-expanded="false">
			<span class="sr-only"> Toggle navigaion</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand logo" href="#"><img src="css/image/logo1.png" alt="logo"></a>
	</div>
</div>
@endsection
@section('content')
<!--_______________________________________ Carousel__________________________________ -->
<div class="allcontain">
	<div id="carousel-up" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner " role="listbox">
			<div class="item active">
				<img src="css/image/oldcar.jpg" alt="oldcar">
				<div class="carousel-caption">
					<h2>Porsche 356</h2>
					<p>Lorem ipsum dolor sit amet, consectetur ,<br>
						sed do eiusmod tempor incididunt ut labore </p>
				</div>
			</div>
			<div class="item">
				<img src="css/image/porche.jpg" alt="porche">
				<div class="carousel-caption">
					<h2>Porche</h2>
					<p>Lorem ipsum dolor sit amet, consectetur ,<br>
						sed do eiusmod tempor incididunt ut labore </p>
				</div>
			</div>
			<div class="item">
				<img src="css/image/benz.jpg" alt="benz">
				<div class="carousel-caption">
					<h2>Car</h2>
					<p>Lorem ipsum dolor sit amet, consectetur ,<br>
						sed do eiusmod tempor incididunt ut labore </p>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-default midle-nav">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed textcostume" data-toggle="collapse"
					data-target="#navbarmidle" aria-expanded="false">
					<h1>SEARCH TEXT</h1>
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
		</nav>
	</div>
</div>
<div class="allcontain">
	<div class="contact">
		<form action="" id="contact_form_action">
			<div class="newslettercontent">
				<div class="leftside">
					<img id="image_border" src="css/image/border.png" alt="border">
					<div class="contact-form">
						<h1>Contact Us</h1>
						<div class="form-group group-coustume contact__group_costume">
							<input type="text" name="name" class="form-control name-form" placeholder="Name">
							<input type="text" name="email" class="form-control email-form" placeholder="E-mail">
							<input type="text" name="subject" class="form-control subject-form" placeholder="Subject">
							<textarea name="text" rows="4" cols="50" class="message-form"></textarea>
							@csrf
						</div>
					</div>
				</div>
				<div class="rightside" data-aos="zoom-in" data-aos-easing="ease-in-out">
					<img class="news/css/image" src="/css/image/newsletter.jpg" alt="newsletter">
					<input type="submit" class="contact_submit_btn" value="SUBMIT">
					<button id="contact__email">EMAIL</button>
					<button id="contact__call">CALL</button>
				</div>
				<h2 style="text-align: center">Find Garage on Google Map</h2>
				<div class="google-maps" data-aos="zoom-out-down">
					<div id="googleMap"></div>
				</div>
			</div>
		</form>

	</div>
</div>
@endsection



<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
	var myCenter=new google.maps.LatLng(41.567197,14.681526);

function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:16,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  });

marker.setMap(map);
}



</script>

<script type="text/javascript" src="css/source/bootstrap-3.3.6-dist/js/jquery.js"></script>
<script type="text/javascript" src="css/source/js/myscript.js"></script>
<script type="text/javascript" src="css/source/bootstrap-3.3.6-dist/js/bootstrap.js"></script>

<script>
	$(window).resize(function(){
		var new_height = $("#image_border").height();
		console.log(new_height);
		$("#googleMap").height(new_height);
	});

	$(window).load(function(){
		var new_height = $("#image_border").height();
		console.log(new_height);
		$("#googleMap").height(new_height);
		google.maps.event.addDomListener(window, 'load', initialize());
	});
	
</script>
<script type="text/javascript" src="css/source/js/myscript.js"></script>
<script type="text/javascript" src="css/source/bootstrap-3.3.6-dist/js/jquery.js"></script>
</body>
</html>