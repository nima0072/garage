@extends('layout')

@section('header')
    <link rel="stylesheet" type="text/css" href="/css/source/bootstrap-5.0.2-dist/css/bootstrap.css">
@endsection

@section('content')
    <div class="container">
        <div class="p-2 border border-primary border-radius-30">
            <form action="/cars/store" method="POST">
                @csrf
                <div class="form-group">
                    <label for="category">Category</label>
                    <select class="form-control" name="category" id="category">
                        <option selected hidden>{{ old("category") }}</option>
                        <option>sport</option>
                        <option>old</option>
                        <option>Special</option>
                    </select>
                    @error('category')
                        <p class="form-text text-muted">{{ $errors->first('category') }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="fullName">Car Name</label>
                    <input type="text" name="fullName" id="fullName" class="form-control" value="{{ old('fullName') }}" aria-describedby="helpId">
                    <small id="helpId" class="text-muted">Enter Car name with exact model</small>
                    @error('carName')
                        <p class="form-text text-muted">{{ $errors->first('fullName') }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="Price">Price</label>
                    <input type="text" class="form-control" name="Price" id="Price" value="{{ old('Price') }}" aria-describedby="helpId2"
                        placeholder="">
                    <small id="helpId2" class="form-text text-muted">make us customer :)</small>
                    @error('Price')
                        <p class="form-text text-muted">{{ $errors->first('Price') }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="topSpeed">Top speed</label>
                    <input type="text" class="form-control" name="topSpeed" id="topSpeed" value="{{ old('topSpeed') }}">
                    @error('topSpeed')
                        <p class="form-text text-muted">{{ $errors->first('topSpeed') }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="imgPath">Picture</label>
                    <select class="form-control" id="imgPath">
                        <option selected hidden> {{old('imgPath')}} </option>
                        <option value="benz">benz</option>
                        <option value="porche">porche</option>
                        <option value="oldcar">old car</option>
                    </select>
                    <img class="border-light m-2 rounded" id="carimage" src="" alt="Car image">
                    <input name="imgPath" id="image" value="{{ old('imgPath') }}" hidden>
                    @error('imgPath')
                        <p class="form-text text-muted">{{ $errors->first('imgPath'); }}</p>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary btn-curved">Post</button>
            </form>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $('#imgPath').on('change', function() {

            var image = $("#imgPath option:selected").val();
            $.ajax({
                url: "/images/" + image,
                method: "get",
                dataType: "text",
                contentType: "application/json",
                success: function(res) {
                    res = res.replace(/"/g, '');
                    load_image(res, "carimage");
                },
                error: function(a, b, error) {
                    alert(error);
                }
            });
        });

        function load_image(src, element_id) {
            src = "/css/image/" + src;
            $("#" + element_id).attr("src", src);
            $("#" + element_id).attr("width", "100");
            $("#" + element_id).attr("heigth", "100");
            $("#image").attr("value", src);
        }
    </script>
@endsection
