<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Scarica gratis GARAGE Template html/css - Web Domus Italia - Web Agency </title>
    <meta name="description"
        content="Scarica gratis il nostro Template HTML/CSS GARAGE. Se avete bisogno di un design per il vostro sito web GARAGE può fare per voi. Web Domus Italia">
    <meta name="author" content="Web Domus Italia">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/source/bootstrap-3.3.6-dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/css/source/font-awesome-4.5.0/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/css/style/slider.css">
    <link rel="stylesheet" type="text/css" href="/css/style/mystyle.css">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />


    @yield('header')

</head>

<body>
    <!-- Header -->
    <div class="allcontain">
        <div class="header">
            <ul class="socialicon">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
            </ul>
            <ul class="givusacall">
                <li>Give us a call : +66666666 </li>
            </ul>
            <ul class="logreg">
                <li><a href="#">Login </a> </li>
                <li><a href="#"><span class="register">Register</span></a></li>
            </ul>
        </div>
        <!-- Navbar Up -->
        <nav class="topnavbar navbar-default topnav">
            @yield('logo')
            <div class="collapse navbar-collapse" id="upmenu">
                <ul class="nav navbar-nav" id="navbarontop">
                    <li class="active"><a href="/">HOME</a> </li>
                    <li class="dropdown">
                        <a href="{{ route("cars") }}" role="button" aria-expanded="false">Advanced Search </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                            aria-expanded="false">CATEGORIES</a>
                        
                        <ul class="dropdown-menu dropdowncostume" id="mydd">
                            <a href="{{ route('cars')}}?category=sport"><li>Sport</li></a>
                            <a href="{{ route('cars')}}?category=special"><li>Special</li></a>
                            <a href="{{ route('cars')}}?category=old"><li>Old</li></a>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="{{ route('dealers') }}">DEALERS </a>

                    </li>
                    <li>
                        <a href="/contact">CONTACT</a>

                    </li>
                    <button><a href="/cars/create"><span class="postnewcar">POST NEW CAR</span></a></button>
                </ul>
            </div>
        </nav>
    </div>

    @yield('content')

    <!-- ______________________________________________________Bottom Menu ______________________________-->
    <div class="bottommenu">
        <div class="bottomlogo">
            <span class="dotlogo">&bullet;</span><img src="/css/image/collectionlogo1.png" alt="logo1"><span
                class="dotlogo">&bullet;;</span>
        </div>
        <ul class="nav nav-tabs bottomlinks">
            <li role="presentation"><a href="#/" role="button">ABOUT US</a></li>
            <li role="presentation"><a href="#/">CATEGORIES</a></li>
            <li role="presentation"><a href="#/">PREORDERS</a></li>
            <li role="presentation"><a href="#/">CONTACT US</a></li>
            <li role="presentation"><a href="#/">RECEIVE OUR NEWSLETTER</a></li>
        </ul>
        <p>"Lorem ipsum dolor sit amet, consectetur, sed do eiusmod tempor incididunt <br>
            eiusmod tempor incididunt </p>
        <img src="/css/image/line.png" alt="line"> <br>
        <div class="bottomsocial">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-google-plus"></i></a>
            <a href="#"><i class="fa fa-pinterest"></i></a>
        </div>
        <div class="footer">
            <div class="copyright">
                &copy; Copy right 2016 | <a href="#">Privacy </a>| <a href="#">Policy</a>
            </div>
            <div class="atisda">
                Designed by <a href="http://www.webdomus.net/">Web Domus Italia - Web Agency </a>
            </div>
        </div>
    </div>
    </div>

    <script type="text/javascript" src="/css/source/bootstrap-3.3.6-dist/js/jquery.js"></script>
    <script type="text/javascript" src="/css/source/js/isotope.js"></script>
    <script type="text/javascript" src="/css/source/js/myscript.js"></script>
    <script type="text/javascript" src="/css/source/bootstrap-3.3.6-dist/js/bootstrap.js"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script type="text/javascript" src="js/main-bundled.js"></script>
    

</body>
@yield('style')
@yield('js')

</html>