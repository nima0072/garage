@extends('layout')

@section('header')
    <link rel="stylesheet" type="text/css" href="/css/source/bootstrap-5.0.2-dist/css/bootstrap.css">
@endsection

@section('content')
    <div class="container">
        <div class="p-2 border border-primary">
            <div class="row">
                <div class="row col-4 order-1 offset-4" id="img">
                    <img class="justify-content-start" src="{{ $car->imgPath }}" alt="Car Image" width="200" height="200" >
                </div>
                <div class="col-4">
                    <div class="card ">
                        <div class="card-body">
                            <h4 class="card-title">{{ $car->fullName }}</h4>
                            <br />
                            <p class="card-text h-3 h5">Price : ${{ $car->Price }} </p>
                        </div>
                        <div class="card-footer d-flex justify-content-center">
                            <form class="col-10" action="#" method="post">

                                <div class="m-3 p-2">
                                    <div class="form-group">
                                      
                                        <input hidden type="text" class="form-control" name="" id="" aria-describedby="helpId"
                                            placeholder="">
                                            <input class="col-12 btn btn-lg btn-success" type="submit" value="Add to Card">                                    
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="container mt-3">
                <div>
                    <table class="table table-bordered table-inverse table-responsive">
                        <thead class="thead-default"></thead>
                            <tbody>
                                <tr>
                                    <td class="col-3" scope="row">Category</td>
                                    <td>{{$car->category}}</td>
                                </tr>
                                <tr>
                                    <td class="col-3" scope="row">Top Speed</td>
                                    <td>{{$car->topSpeed}}</td>
                                </tr>
                            </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
@endsection
