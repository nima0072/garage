@extends('layout')

@section('header')
    <link rel="stylesheet" type="text/css" href="/css/source/bootstrap-5.0.2-dist/css/bootstrap.css">
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @foreach ($dealers as $dealer)
                <div class="col-3 m-4">
                    <div class="border border-dark p-2">
                        <div class="p-3">
                            <h3 class="strong py-2">
                                {{$dealer->fullname}}
                            </h3>
                            <img class="image" src="{{ $dealer->imgPath }}" alt="profile-pic">
                            <br />
                            <p class="col mt-5">
                                {{ $dealer->description }}
                            </p>

                            <div>
                                @foreach ($dealer->cars as $car)
                                    <a href="{{ route('cars.show', $car)}}">{{ $car->fullName }}</a>
                                @endforeach
                            </div>
    
                            
                            <span>popoularity : {{ $dealer->point }} </span> <i class="fa fa-heartbeat fa-lg" aria-hidden="false"></i>
                        </div>
                    </div>    
                </div>                
            @endforeach
        </div>
    </div>
@endsection

@section('style')
    <style>
        .image{
            max-width: 100px;
            max-height: 100px;
        }
    </style>
@endsection