@extends('layout')

@section('header')
    @yield('header')
@endsection

@section('content')

    @yield('search_bar')

    @yield('products')

@endsection
    

