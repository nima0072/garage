<?php
namespace Database\Factories;

use App\Models\cars;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

// factory syntax : App\Models\cars::factory()->create();

class carsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = cars::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'fullName' => $this->faker->name(),
            'category' => $this->faker->randomElement(['sport', 'old', 'special']),
            'Price' => rand(1000,100000),
            'topSpeed' => rand(100, 500),
            'imgPath' => '/css/image/'. $this->faker->randomElement(['car1', 'benz', 'porche']) .'.jpg',
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}