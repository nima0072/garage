<?php

namespace Database\Factories;

use App\Models\dealers;
use Illuminate\Database\Eloquent\Factories\Factory;

class dealersFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = dealers::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'fullname' => $this->faker->name(),
            'point' => $this->faker->randomFloat(2,0,5),
            'imgPath' => '/css/image/'. $this->faker->randomElement(['human1', 'human2', 'human3']) .'.jpg',
            'description' => $this->faker->realText()
        ];
    }
}
