<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CarDealer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_dealers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cars_id');
            $table->unsignedBigInteger('dealers_id');

            $table->unique(['cars_id', 'dealers_id']);

            $table->foreign('cars_id')->references('id')->on('cars')->onDelete('cascade');
            $table->foreign('dealers_id')->references('id')->on('dealers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
