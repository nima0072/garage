<?php

namespace App\Models;

use Database\Factories\carsFactory;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator as PaginationPaginator;
use Illuminate\Support\Facades\DB;
use Nette\Utils\Paginator as UtilsPaginator;

class cars extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function create(array $attributes = [])
    {
        $car_dealer = array();
        $cars_id = DB::select("SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'laravel' AND TABLE_NAME = 'cars'");
        
        $car_dealer['cars_id'] = $cars_id;
        $car_dealer['dealers_id'] = $attributes['dealers_id'];

        car_dealers::create($car_dealer);
       
        $model = static::query()->create($attributes);

        return $model;
    }

    public function dealers()
    {
        return $this->belongsToMany(dealers::class, "car_dealers");
    }
}
