<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use League\CommonMark\Extension\Attributes\Node\Attributes;

class car_dealers extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $guarded = [];

}
