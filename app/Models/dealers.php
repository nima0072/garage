<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class dealers extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function cars()
    {
        return $this->belongsToMany(cars::class, 'car_dealers');

        // many to many realtios always works with method names like cars_id at this example 
        // this is the default of laravel 8.0
        // or u can change it by changing 3nd and 4th args of belongsToMany()
    }
}
