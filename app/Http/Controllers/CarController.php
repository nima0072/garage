<?php

namespace App\Http\Controllers;

use App\Models\cars;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($cars, $pages)
    {
        return view("search_car", ["cars" => $cars, "pages" => $pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create_car');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $car = cars::create($this->validated());

        return redirect(route('cars.show', $car));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(cars $car)
    {
        return view("car" , ["car"=> $car]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_cars()
    {
        $pagination = 9;
        if(request('pagination'))
        {
            $pagination = request('pagination');
        }
        $cars = cars::paginate($pagination);
        
        $pages_count = $this->page_counter($pagination);

        return $this->index($cars, $pages_count);
    }

    public function search_car()
    {
        return $this->get_cars();
    }


    public function validated()
    {
        $validated = request()->validate([
            "fullName" => ["required" , "min:3"],
            "category" => ["required" , "max:8"],
            "topSpeed" => ["required" , "max:1000"],
            "Price" => ["required"],
            "imgPath" => ["required"],
        ]);
        return $validated;
    }

    
    public function page_counter($pagination)
    {
        $count = count(cars::all());
        if($count % $pagination == 0)
        {
            $count -= 1;
        }
        $count =  ($count / $pagination) + 1;
        return $count;
    }

}
