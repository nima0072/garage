<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public static $images = [
        "benz" => "benz.jpg",
        "porche" => "porche.jpg",
        "sport_porche" => "newsletter.jpg",
        "oldcar" => "car1.jpg"
    ];

    public function getImageByName($name)
    {
        $image = $this::$images[$name];
        $res = new JsonResponse($image);
        return $res;
    }
}
