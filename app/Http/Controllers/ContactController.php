<?php

namespace App\Http\Controllers;

use App\Models\feedbacks;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        return view("contact");
    }

    public function emailSender()
    {
        feedbacks::create($this->validated());

    }

    public function validated()
    {
        $req = request()->validate([
            'name' => ['min:3','max:20'],
            'contact' => ['required'],
            'subject' => ['max:100'],
            'feedback' => ['required','max:800']
        ]);
        return $req;
    }
}
