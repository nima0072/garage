<?php

namespace App\Http\Controllers;

use App\Models\dealers;
use Illuminate\Http\Request;

class dealersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dealers = dealers::all();
        return view("dealers", ["dealers" => $dealers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\dealers  $dealers
     * @return \Illuminate\Http\Response
     */
    public function show(dealers $dealers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\dealers  $dealers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, dealers $dealers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\dealers  $dealers
     * @return \Illuminate\Http\Response
     */
    public function destroy(dealers $dealers)
    {
        //
    }
}
