import DOMPurify from "dompurify";
import axios from "axios";
export default class Contact {
    // DOM detection
    constructor() {
        this.form = document.querySelector("#contact_form_action");
        this.name = document.querySelector(".name-form");
        this.email = document.querySelector(".email-form");
        this.subject = document.querySelector(".subject-form");
        this.text = document.querySelector(".message-form");
        this.xsrf_token = document.querySelector("[name=_token]");
        this.events();
    }

    // Setting event listeners

    events() {
        this.form.addEventListener("submit", (e) => {
            e.preventDefault();
            this.post();
        });
    }

    // Methods

    post() {
        const data = {
            name: DOMPurify.sanitize(this.name.value),
            contact: DOMPurify.sanitize(this.email.value),
            subject: DOMPurify.sanitize(this.subject.value),
            feedback: DOMPurify.sanitize(this.text.value),
            _token: this.xsrf_token.value,
        };
        axios
            .post("/contact/send-email", JSON.stringify(data), {
                headers: {
                    "Content-Type": "application/json",
                },
            })
            .then(() => {
                console.log("Success !");
            })
            .catch((e) => {
                console.log("Failed", new Error(e));
            });
    }
}
